﻿using Microsoft.AspNetCore.Http;

namespace ecommerce.ViewModels
{
    public class CreateViewModel
    {   
        public string name { get; set; }

        public string description { get; set; }

        public string category { get; set; }
        public float price { get; set; }
        
        public int quantity { get; set; }
        public bool isNewCollection { get; set; }
        public int categoryId { get; set; }

        public IFormFile Photo { get; set;}
    }
}
