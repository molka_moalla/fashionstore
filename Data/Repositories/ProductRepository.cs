﻿using ecommerce.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        readonly ApplicationDbContext context;
        public ProductRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public Product Add(Product p)
        {
            Console.WriteLine(p.categoryId);
            context.products.Add(p);

            context.SaveChanges();
            return p;
        }

        public Product Delete(Product p)
        {
            Product p1 = context.products.Find(p.id);
            if (p1 != null)
            {
                context.products.Remove(p1);
                context.SaveChanges();
            }
            return p;
        }

        public Product Edit(Product p)
        {
            var product = context.products.Attach(p);
            product.State = EntityState.Modified;
            context.SaveChanges();
            return p;
        }

        public IEnumerable<Product> GetAll()
        {
            return context.products;
        }



        public Product GetProduct(int id)
        {
            return context.products.Find(id);
        }

    
        public int GetProductCategory(int id)
        {
            return context.products.Find(id).categoryId;
        }
        public IEnumerable<Product> products => context.products.Include(c => c.category);
        public IEnumerable<Product> newCollectionProducts => context.products.Where(p => p.isNewCollection).Include(c => c.category);

        public Product GetProductById(int id) => context.products.FirstOrDefault(p => p.id == id);

    }
}
