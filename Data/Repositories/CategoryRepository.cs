﻿using ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        readonly ApplicationDbContext context;

        public CategoryRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public void Add(Category c)
        {
            context.Categories.Add(c);
            context.SaveChanges();
        }

        public void Delete(Category c)
        {
            Category c1 = context.Categories.Find(c.id);
            if (c1 != null)
            {
                context.Categories.Remove(c1);
                context.SaveChanges();
            }
        }

        public void Edit(Category c)
        {
            Category c1 = context.Categories.Find(c.id);
            if (c1 != null)
            {
                c1.name = c.name;

                context.SaveChanges();
            }
        }

        public IEnumerable<Category> GetAll()
        {
            return context.Categories.OrderBy(c => c.name).ToList();
        }


        public Category GetById(int id)
        {
            return context.Categories.Find(id);
        }
        public IEnumerable<Category> Categories => context.Categories;
    }

}


   

