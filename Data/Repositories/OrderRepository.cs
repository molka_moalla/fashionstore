﻿using ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        readonly ApplicationDbContext context;
        readonly ShoppingCart _shoppingCart;
        public OrderRepository(ApplicationDbContext applicationDbContext, ShoppingCart shoppingCart)
        {
            context = applicationDbContext;
            _shoppingCart = shoppingCart;
          
        }
        public void Add(Order o)
        {
            context.Orders.Add(o);
            context.SaveChanges();
        }
        public void Delete(Order o)
        {
            Order o1 = context.Orders.Find(o.id);
            if (o1 != null)
            {
                context.Orders.Remove(o1);
                context.SaveChanges();
            }
        }

        public void Edit(Order o)
        {
            Order o1 = context.Orders.Find(o.id);
            if (o1 != null)
            {

                o1.OrderDate = o.OrderDate;

                context.SaveChanges();
            }
        }

        public IEnumerable<Order> GetAll()
        {
            return context.Orders;
        }


        public Order GetById(int id)
        {
            return context.Orders.Find(id);
        }
        public void CreateOrder(Order order)
        {
            order.OrderDate = DateTime.Now;
            order.OrderTotal = _shoppingCart.GetShoppingCartTotal();
            context.Orders.Add(order);
            context.SaveChanges();

            var shoppingCartItems = _shoppingCart.ShoppingCartItems;

            foreach (var shoppingCartItem in shoppingCartItems)
            {
                var orderDetail = new OrderDetail()
                {
                    Amount = shoppingCartItem.Amount,
                    productId = shoppingCartItem.product.id,
                    orderId = order.id,
                    price = (decimal)shoppingCartItem.product.price

                };

                context.OrderDetails.Add(orderDetail);
            }

            context.SaveChanges();
        }
    }

}


