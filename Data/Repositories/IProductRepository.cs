﻿using ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Data.Repositories
{
    public interface IProductRepository
    {
        Product GetProduct(int id);
        IEnumerable<Product> GetAll();
        Product Add(Product p);
        Product Edit(Product p);
        Product Delete(Product p);
        public IEnumerable<Product> products { get; }
        IEnumerable<Product> newCollectionProducts { get; }

        int GetProductCategory(int id);
    }
}
