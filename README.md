# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Etape pour l'utilisation de notre projet ###

 1. Cloner le dépot sur votre machine local.

 2. Créer une migration
    1. add-migration first migration
    2. update database

 3. Exécuter l'application

 4. Lors de l'exécution vous allez être diriger vers la page d'acceuil de notre site e-commerce où vous aurez 3 choix :
 
     1. Naviguer sur le site en tant que visiteur: Voir tous les produits et leur détails
     2. Naviguer en tant que User: Pour passer une commande 
     3. Naviguer en tant que Admin: Pour gérer les produits, leurs catégories et attribuer les roles aux utilisateurs.
   
Pour essayer le compte Admin vous devez vous connecter avec les coordonnée suivant:


       mail: admin@admin.com
       mdp:  123456
	 

**Remarque:** Si vous voulez ajouter un nouveau compte (s'inscrire), ce dernier ne peut acceder à son panier que si son compte est activé par l'admin 
          (assegner role "User" à ce nouveau compte)
