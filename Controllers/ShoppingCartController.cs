﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ecommerce.Data.Repositories;
using ecommerce.Models;
using ecommerce.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ecommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class ShoppingCartController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ShoppingCart _shoppingCart;

        public ShoppingCartController(IProductRepository productRepository, ShoppingCart shoppingCart)
        {
            _productRepository = productRepository;
            _shoppingCart = shoppingCart;
        }
        // GET: ShoppingCartController
        [Authorize]
        public ViewResult Index()
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()
            };
            return View(shoppingCartViewModel);
        }
        [HttpGet]
        [Authorize]
       
        public RedirectToActionResult AddToShoppingCart(int id)
        {
            Console.WriteLine(id);
            Product selectedProduct = _productRepository.products.FirstOrDefault(p => p.id == id);
           
            if (selectedProduct != null)
            {
                
                _shoppingCart.AddToCart(selectedProduct, 1);
            }
           
            return RedirectToAction("Index");
        }
        [Authorize]
        public RedirectToActionResult RemoveFromShoppingCart(int id)
        {
            var selectedProduct = _productRepository.products.FirstOrDefault(p => p.id == id);
            if (selectedProduct != null)
            {
                _shoppingCart.RemoveFromCart(selectedProduct);
            }
            return RedirectToAction("Index");
        }
    }
}
