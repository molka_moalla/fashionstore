﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ecommerce.Data.Repositories;
using ecommerce.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ecommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ShoppingCart _shoppingCart;
        public OrderController(IOrderRepository orderrepository, ShoppingCart shoppingCart)
        {
            _orderRepository = orderrepository;
            _shoppingCart = shoppingCart;
        }
        // GET: OrderController
        
        public ActionResult Index()
        {
            return View(_orderRepository.GetAll());
        }

        // GET: OrderController/Details/5
        public ActionResult Details(int id)
        {
            var order = _orderRepository.GetById(id);

            return View(order);
        }

        // GET: OrderController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Order o)
        {
            try
            {
                _orderRepository.Add(o);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderController/Edit/5
        public ActionResult Edit(int id)
        {
            var order = _orderRepository.GetById(id);
            return View(order);
        }


        // POST: OrderController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order o)
        {
            try
            {
                _orderRepository.Edit(o);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        // GET: OrderController/Delete/5
        public ActionResult Delete(int id)
        {
            var order = _orderRepository.GetById(id);
            return View(order);
        }

        // POST: OrderController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Order o)
        {
            try
            {
                _orderRepository.Delete(o);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        [Authorize]
        public IActionResult Checkout()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Checkout(Order order)
        {
            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;
            if (_shoppingCart.ShoppingCartItems.Count == 0)
            {
                Console.WriteLine("_shoppingCart.ShoppingCartItems.Count == 0");
                ModelState.AddModelError("", "Votre panier est vide!");
            }

            if (ModelState.IsValid)
            {   _orderRepository.CreateOrder(order);
                _shoppingCart.ClearCart();
                return RedirectToAction("CheckoutComplete");
            }

            return View(order);
        }
        public IActionResult CheckoutComplete()
        {
            ViewBag.CheckoutCompleteMessage = "Votre commande est bien reçue";
            return View();
        }

    }
}
