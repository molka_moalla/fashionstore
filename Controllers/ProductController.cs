﻿using System.IO;
using ecommerce.Models;
using ecommerce.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using ecommerce.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace ecommerce.Controllers

{
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IWebHostEnvironment hostingEnvironment;

        public ProductController(IProductRepository productRepository, ICategoryRepository categoryRepository, IWebHostEnvironment hostingEnvironment)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            this.hostingEnvironment = hostingEnvironment;
        }
        // GET: ProductController
        [AllowAnonymous]
        public ActionResult Index()
        {
            var model = _productRepository.GetAll();


            return View(model);
        }
        [AllowAnonymous]
        public ViewResult List(string category)
        {
            string _category = category;
            IEnumerable<Product> products;
            string currentCategory = string.Empty;

            if (string.IsNullOrEmpty(category))
            {
                products = _productRepository.GetAll().OrderBy(p => p.id);
                currentCategory = "All products";
            }
            else
            {
             
                        if (_categoryRepository.GetAll().Where(p=>p.name.Equals(_category))!=null)
                        {
                            
                            products = _productRepository.GetAll().Where(p => p.category.name.Equals(_category)).OrderBy(p => p.name);
                        }
                else
                {
                     products = _productRepository.GetAll().OrderBy(p => p.id);
                }
                   
                           
                    
               

                currentCategory = _category;
            }

            return View(new ProductsListViewModel
            {
                products = products,
                CurrentCategory = currentCategory
            });
        } 
        // GET: ProductController/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            
            var model = _productRepository.GetProduct(id);
            model.category = _categoryRepository.GetById(_productRepository.GetProductCategory(id));
        
            return View(model);
        }

        // GET: ProductController/Create
        public ActionResult Create()
        {
            ViewBag.categoryId = new SelectList(_categoryRepository.GetAll(), "id", "name");
            return View();
        }

        // POST: ProductController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = null;

                // If the Photo property on the incoming model object is not null, then the user
                // has selected an image to upload.
                if (model.Photo != null)
                {
                    // The image must be uploaded to the images folder in wwwroot
                    // To get the path of the wwwroot folder we are using the inject
                    // HostingEnvironment service provided by ASP.NET Core

                    string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");

                    // To make sure the file name is unique we are appending a new
                    // GUID value and an underscore to the file name

                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);

                    // Use CopyTo() method provided by IFormFile interface to
                    // copy the file to wwwroot/images folder

                    model.Photo.CopyTo(new FileStream(filePath, FileMode.Create));
                }

                Product newProduct = new Product
                {
                    name = model.name,
                    description = model.description,
                    price = model.price,
                    quantity = model.quantity,
                    categoryId = model.categoryId,
                    isNewCollection=model.isNewCollection,
                   
                    //ViewBag.category = new SelectList(_categoryRepository.GetAll(), "SchoolID", "SchoolName");
                    // Store the file name in PhotoPath property of the employee object
                    // which gets saved to the Employees database table
                    imageUrl = uniqueFileName
                };
                ViewBag.categoryId = new SelectList(_categoryRepository.GetAll(), "id", "name",model.categoryId);

                _productRepository.Add(newProduct);
                return RedirectToAction("details", new { newProduct.id });
            }

            return View();
        }

        // GET: ProductController/Edit/5
        public ActionResult Edit(int id)
        {
            Product oldproduct = _productRepository.GetProduct(id);
            ViewBag.categoryId = new SelectList(_categoryRepository.GetAll(), "id", "name");
            ProductEditViewModel productEditViewModel = new ProductEditViewModel
            {
            id= oldproduct.id,
            name = oldproduct.name,
            price = oldproduct.price,
            ExistingimageUrl = oldproduct.imageUrl,
            quantity = oldproduct.quantity,
            description = oldproduct.description,
            isNewCollection = oldproduct.isNewCollection,
            categoryId = oldproduct.categoryId,
            };
           
            return View(productEditViewModel);
        }

        // POST: ProductController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductEditViewModel model) { 
         if (ModelState.IsValid)
            {   // Retrieve the product being edited from the database
                Product product = _productRepository.GetProduct(model.id);
                // Update the employee object with the data in the model object
                product.name = model.name;
                product.price = model.price;
                product.quantity = model.quantity;
                product.imageUrl = model.ExistingimageUrl;
                product.description = model.description;
                product.isNewCollection = model.isNewCollection;
                ViewBag.categoryId = new SelectList(_categoryRepository.GetAll(), "id", "name", model.categoryId);

                product.categoryId = model.categoryId;
                // If the user wants to change the photo, a new photo will be
                // uploaded and the Photo property on the model object receives
                // the uploaded photo. If the Photo property is null, user did
                // not upload a new photo and keeps his existing photo
                if (model.Photo != null){
                    // If a new photo is uploaded, the existing photo must be
                    // deleted. So check if there is an existing photo and delete
                    if (model.ExistingimageUrl != null)
                        {string filePath = Path.Combine(hostingEnvironment.WebRootPath,
                                "images", model.ExistingimageUrl);
                         System.IO.File.Delete(filePath);
                        }
                    // Save the new photo in wwwroot/images folder and update
                    // PhotoPath property of the employee object which will be
                    // eventually saved in the database
                    product.imageUrl= ProcessUploadedFile(model);}

                // Call update method on the repository service passing it the
                // employee object to update the data in the database table
               
                Product updatedProduct = _productRepository.Edit(product);
                    if (updatedProduct != null)
                        return RedirectToAction("index");
                    else
                        return NotFound();
                }
                    return View(model);
                }

    [NonAction]
    private string ProcessUploadedFile(ProductEditViewModel model)
    {
        string uniqueFileName = null;

        if (model.Photo != null)
        {
            string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
            uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
            string filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                model.Photo.CopyTo(fileStream);
            }
        }

        return uniqueFileName;
    }

    // GET: ProductController/Delete/5
    public ActionResult Delete(int id)
        {
            Product p = _productRepository.GetProduct(id);
            if (p == null)
                return NotFound();
            else
                return View(p);
            
        }

        // POST: ProductController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Product p)
        {
            try
            {
                Product p1 =_productRepository.Delete(p);
                if (p1 != null)
                    return RedirectToAction(nameof(Index));
                else
                    return View();
            }
            catch
            {
                return View();
            }
        }
        [AllowAnonymous]
        public ViewResult Search(string searchString)
        {
            string _searchString = searchString;
            
            IEnumerable<Product> products;
            string currentCategory = string.Empty;

            if (string.IsNullOrEmpty(_searchString))
            {
                
                products = _productRepository.GetAll().OrderBy(p => p.id);
            }
            else
            {
                products = _productRepository.GetAll().Where(p => p.name.ToLower().Contains(_searchString.ToLower()));
            }

            return View("~/Views/Product/List.cshtml", new ProductsListViewModel { products = products, CurrentCategory = "Tous les produits" });
        }
        public ViewResult Error(int productId)
        {
            var product = _productRepository.GetAll().FirstOrDefault(d => d.id == productId);
            if (product == null)
            {
                return View("~/Views/Error/Error.cshtml");
            }
            return View(product);
        }
      
    }
}
