﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Models
{
    public class ShoppingCartItem
    {
        public int id { get; set; }
        public Product product { get; set; }
        public int Amount { get; set; }
        public string ShoppingCartId { get; set; }
    }
}
