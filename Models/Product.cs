﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string name { get; set; }

        public float price { get; set; }
        public int quantity { get; set; }
        public bool isNewCollection { get; set; }
        public string description { get; set; }
        [ForeignKey("categoryId")]
        public int categoryId { get; set; }
        public string imageUrl { get; set; }
        public virtual Category category { get; set; }
       
    }
}
