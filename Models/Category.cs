﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ecommerce.Models
{
    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public IEnumerable<Product> proucts { get; set; }
    }
}
